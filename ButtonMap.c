uint8_t RawInput[3] = { 0b11111111, 0b11111111, 0b11111111 };    // raw button inputs before debouncing
uint8_t ButtonState[3] = { 0b11111111, 0b11111111, 0b11111111 }; // button states after debouncing
uint8_t ButtonState_Previous[3] = { 0b11111111, 0b11111111, 0b11111111 };
uint8_t ButtonOutput[2] = { 0b11111111, 0b11111111 };            // button presses currently sent to external encoder

unsigned int DebounceTimer[17] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // time remaining until new state will be accepted for each button
unsigned int DebouncePeriod = 15000; // time to wait after a button state change before accepting a new button state
unsigned int CycleTime = 0; // time elapsed during each cycle (determined at runtime)
// all times in microseconds ("us")

uint8_t ButtonMap[14][3]; // maps ButtonState to ButtonOutput using bit masks

/*
RawInput, ButtonState, and MapSelectButtons:
       index | 2| 1| 1| 1| 1| 1| 1| 1| 1| 0| 0| 0| 0| 0| 0| 0| 0|
         bit | 0| 7| 6| 5| 4| 3| 2| 1| 0| 7| 6| 5| 4| 3| 2| 1| 0|
             ----------------------------------------------------
arcade stick |13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1|Rt|Lf|Dn|Up|
             ----------------------------------------------------
ButtonMap:   |16|15|14|13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1| 0|
             ----------------------------------------------------
Output:  NES          |St|Sl|  |  |  |  |  |  | A| B|Rt|Lf|Dn|Up|
   PC Engine          |Rn|Sl|  |  |  |  |  |  | I|II|Rt|Lf|Dn|Up|
        SNES          |St|Sl| R|  | L|  | X| Y| A| B|Rt|Lf|Dn|Up|
      Saturn          |St| L| C| R| Z| L| Y| X| B| A|Rt|Lf|Dn|Up|
     PSX/PS2          |St|Sl|R2|L2|R1|L1|Tr|Sq|Cr| X|Rt|Lf|Dn|Up|
   Dreamcast          |St|  |CR|  |ZL|  | Y| X| B| A|Rt|Lf|Dn|Up|
    Gamecube          |St| Z| R| Y| L| B| X| Y| A| B|Rt|Lf|Dn|Up|
        XBox          |St|Bk|Bl|RT|Wh|LT| Y| X| B| A|Rt|Lf|Dn|Up|

arcade stick button 11 is map select button
buttons mapped to output button 255 are disabled
*/

// updates ButtonMap according to ButtonMap()
// profile = 1-8 to select specified profile, 0 to select profile according to directionalInput
void ConfigureButtonMap(uint8_t profile)
{
	// if profile = 0, select profile according to directionalInput
	if (profile == 0)
	{
		// read directionalInput
		uint8_t directionalInput = ButtonState[0] | 0b11110000;
		
		// select profile according to directionalInput
		if (directionalInput == 0b11111110)		 // up
			profile = 1;
		else if (directionalInput == 0b11111101) // down
			profile = 2;
		else if (directionalInput == 0b11111011) // left
			profile = 3;
		else if (directionalInput == 0b11110111) // right
			profile = 4;
		else if (directionalInput == 0b11111010) // up+left
			profile = 5;
		else if (directionalInput == 0b11110101) // down+right
			profile = 6;
		else if (directionalInput == 0b11111001) // down+left
			profile = 7;
		else if (directionalInput == 0b11110110) // up+right
			profile = 8;
	}
	
	// define buttonMap
	uint8_t buttonMap[17];
	{
		if (profile == 1) // ButtonMap: Nintendo/Sega/PCE (up, default)
		{
			// === NES ===
			// Button 1 = A
			// Button 2 = B
			// === PC Engine ===
			// Button 1 = I
			// Button 2 = II
			// === SNES ===
			// Button 1 = A
			// Button 2 = B
			// Button 3 = Y
			// Button 4 = X
			// Button 5 = L
			// Button 6 = R
			// === Saturn ===
			// Button 1 = B
			// Button 2 = A
			// Button 3 = X
			// Button 4 = Y
			// Button 5 = Z
			// Button 6 = C
			// Button 7 = L
			// Button 8 = R
			// Button 9 = L
			// === Dreamcast ===
			// Button 1 = B
			// Button 2 = A
			// Button 3 = X
			// Button 4 = Y
			// Button 5 = Z/L
			// Button 6 = C/R
			// === Gamecube ===
			// Button 1 = A
			// Button 2 = B/7
			// Button 3 = Y/8
			// Button 4 = X
			// Button 5 = L
			// Button 6 = R
			// Button 9 = Z
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 5;
			buttonMap[5] = 4;
			buttonMap[6] = 6;
			buttonMap[7] = 7;
			buttonMap[8] = 9;
			buttonMap[9] = 11;
			buttonMap[10] = 8;
			buttonMap[11] = 10;
			buttonMap[12] = 12;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 255;
			buttonMap[16] = 255;
		}
		else if (profile == 2) // ButtonMap: Nintendo/Sega/PCE, buttons 1 and 2 swapped (down)
		{
			// === NES ===
			// Button 1 = B
			// Button 2 = A
			// === PC Engine ===
			// Button 1 = II
			// Button 2 = I
			// === SNES ===
			// Button 1 = B
			// Button 2 = A
			// Button 3 = Y
			// Button 4 = X
			// Button 5 = L
			// Button 6 = R
			// === Saturn ===
			// Button 1 = A
			// Button 2 = B
			// Button 3 = X
			// Button 4 = Y
			// Button 5 = Z
			// Button 6 = C
			// Button 7 = L
			// Button 8 = R
			// Button 9 = L
			// === Dreamcast ===
			// Button 1 = A
			// Button 2 = B
			// Button 3 = X
			// Button 4 = Y
			// Button 5 = Z/L
			// Button 6 = C/R
			// === Gamecube ===
			// Button 1 = B/7
			// Button 2 = A
			// Button 3 = Y/8
			// Button 4 = X
			// Button 5 = L
			// Button 6 = R
			// Button 9 = Z
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 4;
			buttonMap[5] = 5;
			buttonMap[6] = 6;
			buttonMap[7] = 7;
			buttonMap[8] = 9;
			buttonMap[9] = 11;
			buttonMap[10] = 8;
			buttonMap[11] = 10;
			buttonMap[12] = 12;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 255;
			buttonMap[16] = 255;
		}
		else if (profile == 3) // ButtonMap: PS/XBox (left)
		{
			// === PSX/PS2 ===
			// Button 1 = X
			// Button 2 = Circle
			// Button 3 = Square
			// Button 4 = Triangle
			// Button 5 = L1
			// Button 6 = R1
			// Button 7 = L2
			// Button 8 = R2
			// === XBox ===
			// Button 1 = A
			// Button 2 = B
			// Button 3 = X
			// Button 4 = Y
			// Button 5 = LT
			// Button 6 = White
			// Button 7 = RT
			// Button 8 = Black
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 4;
			buttonMap[5] = 5;
			buttonMap[6] = 6;
			buttonMap[7] = 7;
			buttonMap[8] = 8;
			buttonMap[9] = 9;
			buttonMap[10] = 10;
			buttonMap[11] = 11;
			buttonMap[12] = 12;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 255;
			buttonMap[16] = 255;
		}
		else if (profile == 4) // ButtonMap: PS/XBox, buttons 1 and 2 swapped (right)
		{
			// === PSX/PS2 ===
			// Button 1 = Circle
			// Button 2 = X
			// Button 3 = Square
			// Button 4 = Triangle
			// Button 5 = L1
			// Button 6 = R1
			// Button 7 = L2
			// Button 8 = R2
			// === XBox ===
			// Button 1 = B
			// Button 2 = A
			// Button 3 = X
			// Button 4 = Y
			// Button 5 = LT
			// Button 6 = White
			// Button 7 = RT
			// Button 8 = Black
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 5;
			buttonMap[5] = 4;
			buttonMap[6] = 6;
			buttonMap[7] = 7;
			buttonMap[8] = 8;
			buttonMap[9] = 9;
			buttonMap[10] = 10;
			buttonMap[11] = 11;
			buttonMap[12] = 12;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 255;
			buttonMap[16] = 255;
		}
		else if (profile == 5) // ButtonMap: Nintendo/Sega, 6-button (up+left)
		{
			// === SNES ===
			// Button 1 = Y
			// Button 2 = X
			// Button 3 = L
			// Button 5 = B
			// Button 6 = A
			// Button 7 = R
			// === Saturn ===
			// Button 1 = X
			// Button 2 = Y
			// Button 3 = Z
			// Button 5 = A
			// Button 6 = B
			// Button 7 = C
			// === Dreamcast ===
			// Button 1 = X
			// Button 2 = Y
			// Button 3 = Z/L
			// Button 5 = A
			// Button 6 = B
			// Button 7 = C/R
			// === Gamecube ===
			// Button 1 = Y
			// Button 2 = X
			// Button 3 = L
			// Button 5 = B
			// Button 6 = A
			// Button 7 = R
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 6;
			buttonMap[5] = 7;
			buttonMap[6] = 9;
			buttonMap[7] = 255;
			buttonMap[8] = 4;
			buttonMap[9] = 5;
			buttonMap[10] = 11;
			buttonMap[11] = 255;
			buttonMap[12] = 12;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 255;
			buttonMap[16] = 255;
		}
		if (profile == 6) // ButtonMap: NES/PCE Shmups, buttons 3 and 9 swapped (down+right)
		{
			// === NES ===
			// Button 1 = A
			// Button 2 = B
			// Button 3 = Select
			// === PC Engine ===
			// Button 1 = I
			// Button 2 = II
			// Button 3 = Select
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 5;
			buttonMap[5] = 4;
			buttonMap[6] = 12;
			buttonMap[7] = 7;
			buttonMap[8] = 9;
			buttonMap[9] = 11;
			buttonMap[10] = 8;
			buttonMap[11] = 10;
			buttonMap[12] = 6;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 255;
			buttonMap[16] = 255;
		}
		else if (profile == 7) // ButtonMap: NES/PCE Shmups, buttons 1 and 2, 3 and 9 swapped (down+left)
		{
			// === NES ===
			// Button 1 = B
			// Button 2 = A
			// Button 3 = Select
			// === PC Engine ===
			// Button 1 = II
			// Button 2 = I
			// Button 3 = Select
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 4;
			buttonMap[5] = 5;
			buttonMap[6] = 12;
			buttonMap[7] = 7;
			buttonMap[8] = 9;
			buttonMap[9] = 11;
			buttonMap[10] = 8;
			buttonMap[11] = 10;
			buttonMap[12] = 6;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 255;
			buttonMap[16] = 255;
		}
		else if (profile == 8) // ButtonMap: Alien Crush/Devil's Crush (up+right)
		{
			// === PC Engine ===
			// Button 14   = Down (left flippers)
			// Button 15   = I    (right flippers)
			// Buttons 1-8 = II   (tilt)
			buttonMap[0] = 0;
			buttonMap[1] = 1;
			buttonMap[2] = 2;
			buttonMap[3] = 3;
			buttonMap[4] = 4;
			buttonMap[5] = 4;
			buttonMap[6] = 4;
			buttonMap[7] = 4;
			buttonMap[8] = 4;
			buttonMap[9] = 4;
			buttonMap[10] = 4;
			buttonMap[11] = 4;
			buttonMap[12] = 12;
			buttonMap[13] = 13;
			buttonMap[14] = 255;
			buttonMap[15] = 1;
			buttonMap[16] = 5;
		}
	}
	
	// default ButtonMap to empty
	for (uint8_t button = 0; button < 14; button++)
		for (uint8_t index = 0; index < 3; index++)
			ButtonMap[button][index] = 0b11111111;
	
	// convert buttonMap to ButtonMap
	for (uint8_t button = 0; button < 17; button++)
		if (buttonMap[button] < 14)
		{
			uint8_t bit = button;
			uint8_t index = 0;
			
			// adjust bit and index to select position in ButtonMap
			while (bit > 7)
			{
				bit -= 8;
				index++;
			}
			
			ButtonMap[buttonMap[button]][index] = ButtonMap[buttonMap[button]][index] & ~(0b0000001 << bit);
		}
}