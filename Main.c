/*
Cthulhu Encoder v1.2
Arcade stick decoder with passthrough to Multi-Console Cthulhu encoder
14-DEC-2021

Written by Robert Lloyd
rl636018@gmail.com

In Makefile, set MCU to device and F_CPU to clock speed
In Main.c, set CLKPR according to clock speed

intended for compilation using avr-gcc, tested with v7.3.0
*/

#include <avr/io.h>
#include <util/delay.h>
#include "ButtonMap.c"

void ReadWrite(void);
void Debounce(void);
void Update(void);

int main(void)
{
	CLKPR = 0x80, CLKPR = 0x00; // set clock prescale for 16 MHz clock
	
	// set I/O registers
	// -----------------
	// B0-7	to Chtulhu (output)
	// C0-1	unused
	// C2	controller D4 (input)
	// C3	unused
	// C4-7	controller D0-3 (input)
	// D0-5	to Chtulhu (output)
	// D6	controller D5 (output)
	// D7	controller D6 (output)
	// enable pull-up resistors for inputs and default outputs high
	
	DDRB = 0b11111111;
	DDRC = 0b00000000;
	DDRD = 0b11111111;
	
	PORTB = 0b11111111;
	PORTC = 0b11111111;
	PORTD = 0b11111111;
	
	// apply default ButtonMap
	ConfigureButtonMap(1);
	
    // main input loop
	while(1)
	{
		// read RawInput from arcade stick and write ButtonOutputs to MC Cthulhu
		ReadWrite();
		
		// debounce RawInput to determine ButtonState
		Debounce();
		
		// if map select button is pressed, update ButtonMap
		if ((ButtonState[1] & 0b01000000) == 0)
			ConfigureButtonMap(0);
		
		// update ButtonOutput according to ButtonState and ButtonMap
		Update();
	}
}

// reads RawInput from arcade stick and writes ButtonOutputs to MC Cthulhu
void ReadWrite(void)
{
	PORTB = ButtonOutput[0];
	PORTD = ButtonOutput[1] & 0b00111111;
	_delay_us(3);
	RawInput[0] = PINC >> 4;
	
	PORTD = ButtonOutput[1] & 0b01111111;
	_delay_us(3);
	RawInput[0] = RawInput[0] | (PINC & 0b11110000);
	
	PORTD = ButtonOutput[1] & 0b10111111;
	_delay_us(3);
	RawInput[1] = PINC >> 4;
	
	PORTD = ButtonOutput[1] & 0b11111111;
	_delay_us(3);
	{
		uint8_t pinc = PINC;
		
		RawInput[1] = RawInput[1] | (pinc & 0b11110000);
		RawInput[2] = (pinc >> 2) | 0b11111110;
	}
}

// debounces RawInput and determines ButtonState
void Debounce(void)
{
	// update DebounceTimer for each button according to CycleTime
	for (uint8_t button = 0; button < 17; button++)
	{
		if (DebounceTimer[button] >= CycleTime)
			DebounceTimer[button] -= CycleTime;
		else if (DebounceTimer[button] > 0)
			DebounceTimer[button] = 0;
	}
	
	// reset CycleTime to baseline
	CycleTime = 43;
	
	// if RawInput[0] is not equal to ButtonState[0],
	//   update ButtonState[0]
	//   record increase in CycleTime
	if (RawInput[0] != ButtonState[0])
	{
		// for each button in RawInput[0],
		//   if DebounceTimer has reached 0 and bit in RawInput[0] has changed,
		//     update bit in ButtonState[0]
		//     reset DebounceTimer
		for (uint8_t buttonBit = 0; buttonBit < 8; buttonBit++)
			if ((DebounceTimer[buttonBit] == 0)
				&& ((ButtonState[0] & (0b00000001 << buttonBit)) != (RawInput[0] & (0b00000001 << buttonBit))))
			{
				ButtonState[0] = (ButtonState[0] & ~(0b00000001 << buttonBit)) | RawInput[0];
				
				DebounceTimer[buttonBit] = DebouncePeriod;
			}
		
		CycleTime += 22;
	}
	
	// if RawInput[1] is not equal to ButtonState[1],
	//   update ButtonState[1]
	//   record increase in CycleTime
	if (RawInput[1] != ButtonState[1])
	{
		// for each button in RawInput[1],
		//   if DebounceTimer has reached 0 and bit in RawInput[1] has changed,
		//     update bit in ButtonState[1]
		//     reset DebounceTimer
		for (uint8_t buttonBit = 0; buttonBit < 8; buttonBit++)
			if ((DebounceTimer[buttonBit + 8] == 0)
				&& ((ButtonState[1] & (0b00000001 << buttonBit)) != (RawInput[1] & (0b00000001 << buttonBit))))
			{
				ButtonState[1] = (ButtonState[1] & ~(0b00000001 << buttonBit)) | RawInput[1];
				
				DebounceTimer[buttonBit + 8] = DebouncePeriod;
			}
		
		CycleTime += 22;
	}
	
	// if DebounceTimer[16] has reached 0 and RawInput[2] has changed,
	//   update ButtonState[2]
	//   reset DebounceTimer
	if ((DebounceTimer[16] == 0) && (ButtonState[2] != RawInput[2]))
	{
		ButtonState[2] = RawInput[2];
		
		DebounceTimer[16] = DebouncePeriod;
	}
}

// updates ButtonOutput according to ButtonState and ButtonMap
void Update(void)
{
	// if ButtonState has changed,
	//   update ButtonOutput
	//   record increase in CycleTime
	if ((ButtonState[0] != ButtonState_Previous[0])
		|| (ButtonState[1] != ButtonState_Previous[1])
		|| (ButtonState[2] != ButtonState_Previous[2]))
	{
		ButtonState_Previous[0] = ButtonState[0];
		ButtonState_Previous[1] = ButtonState[1];
		ButtonState_Previous[2] = ButtonState[2];
		
		// default ButtonOutput to none pressed
		ButtonOutput[0] = 0b11111111;
		ButtonOutput[1] = 0b11111111;
		
		if (((ButtonState[0] | ButtonMap[0][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[0][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[0][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11111110;
		
		if (((ButtonState[0] | ButtonMap[1][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[1][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[1][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11111101;
		
		if (((ButtonState[0] | ButtonMap[2][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[2][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[2][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11111011;
		
		if (((ButtonState[0] | ButtonMap[3][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[3][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[3][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11110111;
		
		if (((ButtonState[0] | ButtonMap[4][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[4][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[4][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11101111;

		if (((ButtonState[0] | ButtonMap[5][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[5][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[5][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b11011111;

		if (((ButtonState[0] | ButtonMap[6][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[6][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[6][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b10111111;

		if (((ButtonState[0] | ButtonMap[7][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[7][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[7][2]) != 0b11111111))
			ButtonOutput[0] = ButtonOutput[0] & 0b01111111;
		
		if (((ButtonState[0] | ButtonMap[8][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[8][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[8][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11111110;
		
		if (((ButtonState[0] | ButtonMap[9][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[9][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[9][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11111101;
		
		if (((ButtonState[0] | ButtonMap[10][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[10][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[10][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11111011;
		
		if (((ButtonState[0] | ButtonMap[11][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[11][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[11][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11110111;
		
		if (((ButtonState[0] | ButtonMap[12][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[12][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[12][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11101111;

		if (((ButtonState[0] | ButtonMap[13][0]) != 0b11111111)
			|| ((ButtonState[1] | ButtonMap[13][1]) != 0b11111111)
			|| ((ButtonState[2] | ButtonMap[13][2]) != 0b11111111))
			ButtonOutput[1] = ButtonOutput[1] & 0b11011111;	
		
		CycleTime += 15;
	}
}