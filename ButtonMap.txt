ButtonMap: Nintendo/Sega/PCE (up, default)
    === NES ===
    Button 1 = A
    Button 2 = B
    === PC Engine ===
    Button 1 = I
    Button 2 = II
    === SNES ===
    Button 1 = A
    Button 2 = B
    Button 3 = Y
    Button 4 = X
    Button 5 = L
    Button 6 = R
    === Saturn ===
    Button 1 = B
    Button 2 = A
    Button 3 = X
    Button 4 = Y
    Button 5 = Z
    Button 6 = C
    Button 7 = L
    Button 8 = R
    Button 9 = L
    === Dreamcast ===
    Button 1 = B
    Button 2 = A
    Button 3 = X
    Button 4 = Y
    Button 5 = Z/L
    Button 6 = C/R
    === Gamecube ===
    Button 1 = A
    Button 2 = B/7
    Button 3 = Y/8
    Button 4 = X
    Button 5 = L
    Button 6 = R
    Button 9 = Z

-

ButtonMap: Nintendo/Sega/PCE, buttons 1 and 2 swapped (down)
    === NES ===
    Button 1 = B
    Button 2 = A
    === PC Engine ===
    Button 1 = II
    Button 2 = I
    === SNES ===
    Button 1 = B
    Button 2 = A
    Button 3 = Y
    Button 4 = X
    Button 5 = L
    Button 6 = R
    === Saturn ===
    Button 1 = A
    Button 2 = B
    Button 3 = X
    Button 4 = Y
    Button 5 = Z
    Button 6 = C
    Button 7 = L
    Button 8 = R
    Button 9 = L
    === Dreamcast ===
    Button 1 = A
    Button 2 = B
    Button 3 = X
    Button 4 = Y
    Button 5 = Z/L
    Button 6 = C/R
    === Gamecube ===
    Button 1 = B/7
    Button 2 = A
    Button 3 = Y/8
    Button 4 = X
    Button 5 = L
    Button 6 = R
    Button 9 = Z

-

ButtonMap: PS/XBox (left)
    === PSX/PS2 ===
    Button 1 = X
    Button 2 = Circle
    Button 3 = Square
    Button 4 = Triangle
    Button 5 = L1
    Button 6 = R1
    Button 7 = L2
    Button 8 = R2
    === XBox ===
    Button 1 = A
    Button 2 = B
    Button 3 = X
    Button 4 = Y
    Button 5 = LT
    Button 6 = White
    Button 7 = RT
    Button 8 = Black

-

ButtonMap: PS/XBox, buttons 1 and 2 swapped (right)
    === PSX/PS2 ===
    Button 1 = Circle
    Button 2 = X
    Button 3 = Square
    Button 4 = Triangle
    Button 5 = L1
    Button 6 = R1
    Button 7 = L2
    Button 8 = R2
    === XBox ===
    Button 1 = B
    Button 2 = A
    Button 3 = X
    Button 4 = Y
    Button 5 = LT
    Button 6 = White
    Button 7 = RT
    Button 8 = Black

-

ButtonMap: Nintendo/Sega, 6-button (up+left)
    === SNES ===
    Button 1 = Y
    Button 2 = X
    Button 3 = L
    Button 5 = B
    Button 6 = A
    Button 7 = R
    === Saturn ===
    Button 1 = X
    Button 2 = Y
    Button 3 = Z
    Button 5 = A
    Button 6 = B
    Button 7 = C
    === Dreamcast ===
    Button 1 = X
    Button 2 = Y
    Button 3 = Z/L
    Button 5 = A
    Button 6 = B
    Button 7 = C/R
    === Gamecube ===
    Button 1 = Y
    Button 2 = X
    Button 3 = L
    Button 5 = B
    Button 6 = A
    Button 7 = R

-

ButtonMap: NES/PCE Shmups, buttons 3 and 9 swapped (down+right)
    === NES ===
    Button 1 = A
    Button 2 = B
    Button 3 = Select
    === PC Engine ===
    Button 1 = I
    Button 2 = II
    Button 3 = Select

-

ButtonMap: NES/PCE Shmups, buttons 1 and 2, 3 and 9 swapped (down+left)
    === NES ===
    Button 1 = B
    Button 2 = A
    Button 3 = Select
    === PC Engine ===
    Button 1 = II
    Button 2 = I
    Button 3 = Select

-

ButtonMap: Alien Crush/Devil's Crush (up+right)
    === PC Engine ===
    Button 14   = Down (left flippers)
    Button 15   = I    (right flippers)
    Buttons 1-8 = II   (tilt)