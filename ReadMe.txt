Cthulhu Encoder 
Arcade stick decoder with passthrough to Multi-Console Cthulhu encoder

Written by Robert Lloyd
rl636018@gmail.com


This project allows an arcade stick using a 17 Input Button Encoder (see repository 17InputButtonEncoder) to connect to a Multi-Console Cthulhu encoder.  Designed for ATMEGA32U2.

Features:

- Button maps may selected at any time by holding button 11 and a joystick direction.

- Button maps may be updated in ButtonMap.c.

- Button map summary (ButtonMap.txt) may be generated by running ButtonMapSummaryCompiler.exe.
    (see repository "ButtonMapSummaryCompiler" for source code)